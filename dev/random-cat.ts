

export default class RandomCat {
  /*
   * Метод получения случайного имени для кота
   */
  static generateName(strLength: number = 5, dataSet: string = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя') {
    let randomString = "";
    for (let i = 0; i < strLength; i++)
    {
      randomString += dataSet.charAt(Math.floor(Math.random() * dataSet.length));
    }
    return "Котиккотик " + randomString;
  }
}
