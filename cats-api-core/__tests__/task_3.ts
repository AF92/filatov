import Client from '../../dev/http-client';
import type { Reporter } from 'jest-allure/dist/Reporter';
import { Severity } from 'jest-allure/dist/Reporter';

declare const reporter: Reporter;

// можно попробовать заменить хардкод на рандом. limit (от 1 до 10, например. какие лимиты в документации), order (между asc и desc), gender (между male, female, unisex)
const G_LIMIT = 3;
const G_ORDER = "asc";
const G_GENDER = "female";

const HTTP_CLIENT = Client.getInstance();

describe('3 задание ДЗ', () => {
  it('Получение списка котов сгруппированных по группам', async () => {
    reporter.severity(Severity.Blocker);
    reporter.description('Это тест на проверку распределения котиков по группам #allByLetter#');

    reporter.startStep('Делаю запрос на получение котиков по группам');
    const response = await HTTP_CLIENT.get(`core/cats/allByLetter?limit=${G_LIMIT}&order=${G_ORDER}&gender=${G_GENDER}`, {
      responseType: 'json',
    });
    reporter.addAttachment(
      'testAttachment',
      JSON.stringify(response.body, null, 2),
      'application/json'
    );
    reporter.endStep();


    // ответ сервера == 200
    reporter.startStep('Делаю проверку статуса');
    expect(response.statusCode).toEqual(200);
    reporter.endStep();
    
    // проходимся по группам в ответе
    reporter.startStep('Обходим массив groups для проверки модели, количества, распределения по буквам');
    response.body["groups"].forEach(function(group) {
      let catCount = 0;
      // проходим по каждому коту в группе
      group.cats.forEach(function(catTemp) {
        // проверяем модель кота
        expect(catTemp).toMatchObject({
          id: expect.any(Number),
          name: expect.any(String)
        });

        // проверяем, что коты правильно распределены по группам. т.е. первая буква в имени кота === имя группы
        expect(catTemp.name.startsWith(group.title)).toEqual(true);

        // get параметр gender работает корректно
        expect(catTemp.gender).toEqual(G_GENDER);
        
        catCount++;
      });

      // get параметр limit работает корректно
      expect(group.count_in_group === catCount).toEqual(true);
    });
    reporter.endStep();

    // параметры count_output и count_all на месте
    reporter.startStep('Делаю проверку модели ответа, поля count_output и count_all');
    expect(response.body).toMatchObject({
      count_output: expect.any(Number),
      count_all: expect.any(Number)
    });
    reporter.endStep();

  });
  
});
