import Client from '../../dev/http-client';
import RandomCat from '../../dev/random-cat';
import type { CatMinInfo, CatsList } from '../../dev/types';
import type { Reporter } from 'jest-allure/dist/Reporter';
import { Severity } from 'jest-allure/dist/Reporter';

declare const reporter: Reporter;

const CATS_DEF: CatMinInfo[] = [{ name: RandomCat.generateName(), description: '', gender: 'male' }];

let catId;
const FAKE_ID = 'test123';
const HTTP_CLIENT = Client.getInstance();

describe('1 задание ДЗ', () => {
  beforeAll(async () => {
    // создаем тестового кота
    try {
      const add_cat_response = await HTTP_CLIENT.post('core/cats/add', {
        responseType: 'json',
        json: { cats: CATS_DEF },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    // удаляем тестового кота
    await HTTP_CLIENT.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  // тест на поиск существующего котика по id
  it('Поиск существующего котика по id', async () => {
    reporter.severity(Severity.Blocker);
    reporter.description('Это тест на проверку получения котика по существующему id (id - тестовый созданный) #get-by-id#');

    reporter.startStep('Делаю запрос на получение по id');
    const response = await HTTP_CLIENT.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    reporter.addAttachment(
      'testAttachment',
      JSON.stringify(response.body, null, 2),
      'application/json'
    );
    reporter.endStep();

    // ответ сервера == 200
    reporter.startStep('Делаю проверку статуса');
    expect(response.statusCode).toEqual(200);
    reporter.endStep();

    // проверяем модель кота
    reporter.startStep('Делаю проверку модели кота');
    expect(response.body).toMatchObject({
      cat: expect.objectContaining({
        id: catId,
        ...CATS_DEF[0]
      }),
    });
    reporter.endStep();
  });
  
  // тест на поиск котика по некорректному id
  it('Поиск котика по некорректному id', async () => {
    reporter.severity(Severity.Normal);
    reporter.description('Это тест на проверку получения котика по некорректному id, вернет 400 статус #get-by-id#');


    // ответ сервера == 400
    reporter.startStep('Делаю проверку статуса');
    await expect(
      HTTP_CLIENT.get(`core/cats/get-by-id?id=${FAKE_ID}`, {
        responseType: 'json',
      })
    ).rejects.toThrowError('Response code 400 (Bad Request)');

    // start status сейчас я бы сделал проверку 400 статуса так (подсмотрев у другого ученика =)):
    /*
    try {
      await HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
        responseType: 'json',
      });
    } catch (error) {
      expect(error.response.statusCode).toEqual(400);
    }
    */
    // end status
    
    
    reporter.endStep();
  });

});
