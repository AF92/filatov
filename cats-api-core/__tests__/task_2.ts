import Client from '../../dev/http-client';
import RandomCat from '../../dev/random-cat';
import type { CatMinInfo, CatsList } from '../../dev/types';
import type { Reporter } from 'jest-allure/dist/Reporter';
import { Severity } from 'jest-allure/dist/Reporter';

declare const reporter: Reporter;

const CATS_DEF: CatMinInfo[] = [{ name: RandomCat.generateName(), description: '', gender: 'male' }];

let catId;
const CAT_DESC = "Описание тестовое автоматизированное";

const HTTP_CLIENT = Client.getInstance();

describe('2 задание ДЗ', () => {
  beforeAll(async () => {
    // создаем тестового кота
    try {
      const add_cat_response = await HTTP_CLIENT.post('core/cats/add', {
        responseType: 'json',
        json: { cats: CATS_DEF },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    // удаляем тестового кота
    await HTTP_CLIENT.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Добавление описания существующему котику', async () => {
    reporter.severity(Severity.Normal);
    reporter.description('Это тест на проверку обновления description котика #save-description#');

    reporter.startStep('Делаю запрос на обновление description котика');
    const response = await HTTP_CLIENT.post(`core/cats/save-description`, {
      responseType: 'json',
      json: {
        "catId": catId,
        "catDescription": CAT_DESC
      },
    });
    reporter.addAttachment(
      'testAttachment',
      JSON.stringify(response.body, null, 2),
      'application/json'
    );
    reporter.endStep();


    // ответ сервера == 200
    reporter.startStep('Делаю проверку статуса');
    expect(response.statusCode).toEqual(200);
    reporter.endStep();

    // проверяем модель - что id тот же, description установлен
    reporter.startStep('Делаю проверку модели кота');
    expect(response.body).toMatchObject({
      id: catId,
      description: CAT_DESC,
    });
    reporter.endStep();

    // доп. проверка запрос - что описание точно установлено. Тест становится зависимым от другого запроса. Но в то же время и более надежным (при условии работы get-by-id)
    reporter.startStep('Делаю дополнительный запрос get-by-id для проверки description');
    const responseGet = await HTTP_CLIENT.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    expect(responseGet.body).toEqual({
      cat: expect.objectContaining({
        id: catId,
        description: CAT_DESC,
      }),
    });
    reporter.endStep();
    
  });
  

});
