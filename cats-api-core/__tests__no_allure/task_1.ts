import Client from '../../dev/http-client';
import RandomCat from '../../dev/random-cat';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: RandomCat.generateName(), description: '', gender: 'male' }];

let catId;
const fakeId = 'test123';
const HttpClient = Client.getInstance();

describe('1 задание ДЗ', () => {
  beforeAll(async () => {
    // создаем тестового кота
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    // удаляем тестового кота
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  // тест на поиск существующего котика по id
  it('Поиск существующего котика по id (id - тестовый созданный) #get-by-id#', async () => {
    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    // ответ сервера == 200
    expect(response.statusCode).toEqual(200);

    // проверяем модель кота
    expect(response.body).toMatchObject({
      cat: expect.objectContaining({
        id: catId,
        ...cats[0]
      }),
    });
  });
  
  // тест на поиск котика по некорректному id
  it('Поиск котика по некорректному id вернет 400 статус #get-by-id#', async () => {
    // ответ сервера == 400
    await expect(
      HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
        responseType: 'json',
      })
    ).rejects.toThrowError('Response code 400 (Bad Request)');
  });

});
