import Client from '../../dev/http-client';
import RandomCat from '../../dev/random-cat';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: RandomCat.generateName(), description: '', gender: 'male' }];

let limit = 3;
let order = "asc";
let gender = "female";
const HttpClient = Client.getInstance();

describe('3 задание ДЗ', () => {
  it('Получение списка котов сгруппированных по группам #allByLetter#', async () => {
    const response = await HttpClient.get(`core/cats/allByLetter?limit=${limit}&order=${order}&gender=${gender}`, {
      responseType: 'json',
    });
    // ответ сервера == 200
    expect(response.statusCode).toEqual(200);
    
    // проходимся по группам в ответе
    response.body["groups"].forEach(function(group) {
      let catCount = 0;
      // проходим по каждому коту в группе
      group.cats.forEach(function(catTemp) {
        // проверяем модель кота
        expect(catTemp).toMatchObject({
          id: expect.any(Number),
          name: expect.any(String)
        });

        // проверяем, что коты правильно распределены по группам. т.е. первая буква в имени кота === имя группы
        expect(catTemp.name.charAt(0) === group.title).toEqual(true);

        // get параметр gender работает корректно
        expect(catTemp.gender).toEqual(gender);
        
        catCount++;
      });

      // get параметр limit работает корректно
      expect(group.count_in_group === catCount).toEqual(true);
    });

    // параметры count_output и count_all на месте
    expect(response.body).toMatchObject({
      count_output: expect.any(Number),
      count_all: expect.any(Number)
    });

  });
  
});
