import Client from '../../dev/http-client';
import RandomCat from '../../dev/random-cat';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: RandomCat.generateName(), description: '', gender: 'male' }];

let catId;
const catDesc = "Описание тестовое автоматизированное";

const HttpClient = Client.getInstance();

describe('2 задание ДЗ', () => {
  beforeAll(async () => {
    // создаем тестового кота
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    // удаляем тестового кота
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Добавление описания существующему котику #save-description#', async () => {
    const response = await HttpClient.post(`core/cats/save-description`, {
      responseType: 'json',
      json: {
        "catId": catId,
        "catDescription": catDesc
      },
    });
    // ответ сервера == 200
    expect(response.statusCode).toEqual(200);

    // проверяем модель - что id тот же, description установлен
    expect(response.body).toMatchObject({
      id: catId,
      description: catDesc,
    });

    // доп. проверка запрос - что описание точно установлено. Тест становится зависимым от другого запроса. Но в то же время и более надежным (при условии работы get-by-id)
    const responseGet = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });
    expect(responseGet.body).toEqual({
      cat: expect.objectContaining({
        id: catId,
        description: catDesc,
      }),
    });
    
  });
  

});
